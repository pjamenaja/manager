#!/usr/bin/php

<?php

require_once "phar://onix_core_framework.phar/onix_core_include.php";

CloudBase::SetDebugMode(true);

$PROJECT_ID = 'compute-engine-vm-test';

$USER_NAME = 'SEUBPONG';
$USER_PASSWORD = '1234E09A244aynx'; #Will be supplied on the fly

$PRODUCT_NAME = 'onix';
$STAGE = 'dev';
$SHORT_NAME = 'wtt';
$LONG_NAME = 'test';
$DNS = "$PRODUCT_NAME-$STAGE-$SHORT_NAME-$LONG_NAME";
$URL_PATH = "$PRODUCT_NAME/$STAGE/$SHORT_NAME/$LONG_NAME";
$WS_URL = "https://%s/$URL_PATH/dispatcher.php";
$DL_URL = "https://%s/$URL_PATH/install/OnixCenter.zip";

$IMAGE = 'centos-7-v20180815';
$IMAGE_PROJECT = 'centos-cloud';
$ZONE = 'asia-east1-b';
$HOME_DIR = '/home/pjame_fb';
$CUSTOM_IMAGES = ['master' => 'master-20180904-084647pm', 'worker' => 'worker-20180904-084850pm', 'storage' => 'storage-20180904-085106pm'];

$STORAGE_INSTANCE = 'storage-000:g1-small:10GB:pd-standard';
$WORKER_INSTANCE = ['worker-000:g1-small:10GB:pd-standard'];
$MASTER_INSTANCE = ['master-000:g1-small:10GB:pd-standard'];

$DELAY_SEC = 10;
$PARAM = [];

list($storageInstance) = createStorageInstance($STORAGE_INSTANCE);
$_ENV{'STORAGE_INSTANCE'} = $storageInstance;

$masters = createLBInstances($MASTER_INSTANCE);
$workers = createLBInstances($WORKER_INSTANCE);

#$masters = ['onix-dev-wtt-test-master-000'];
#$masterImg = createImage($masters[0], 'master');
#$workerImg = createImage($workers[0], 'worker');
#$storageImg = createImage($storageInstance, 'storage');
#exit(0);

#$storageInstance = 'onix-dev-wtt-test001-storage-000';
$cfg = "./$PRODUCT_NAME.$STAGE.$SHORT_NAME.$LONG_NAME.cfg";
initialConfig($storageInstance, $cfg);

#initSwarm(['onix-dev-wtt-develop-master-000'], ['onix-dev-wtt-develop-worker-000', 'onix-dev-wtt-develop-worker-001']);
#deployApp('onix-dev-wtt-develop-master-000', 'onix-dev-wtt-develop-storage-000', $PRODUCT_NAME);

initSwarm($masters, $workers);
deployApp($masters[0], $storageInstance, $PRODUCT_NAME);
setDNS($masters[0], $DNS);

displayParam($PARAM);

#Wait for service to start
sleep($DELAY_SEC * 2);
initDB($masters[0]);


exit(0);

function createImage($name, $imageKey)
{
    global $PROJECT_ID, $ZONE;

    $data = new CTable('');
    $data->setFieldValue('ID', $name);
    $data->setFieldValue('PROJECT', $PROJECT_ID);
    $data->setFieldValue('ZONE', $ZONE);

    list($result, $lines, $output) = GcpVM::StopVM($data);
    if ($result != 0)
    {
        print($output);
        exit(1);
    }

    date_default_timezone_set('Asia/Bangkok');
    $dt = date("Ymd-hisa");

    $imageName = sprintf('%s-%s', $imageKey, $dt);
    $image = new CTable('');
    $image->setFieldValue('ID', $imageName);
    $image->setFieldValue('SOURCE_DISK', $name);
    $image->setFieldValue('SOURCE_DISK_ZONE', $ZONE);
    list($result, $lines, $output) = GcpImage::CreateImageFromDisk($image);
    if ($result != 0)
    {
        print($output);
        exit(1);
    }

    list($result, $lines, $output) = GcpVM::StartVM($data);
    if ($result != 0)
    {
        print($output);
        exit(1);
    }

    return($imageName);
}

function displayParam($params)
{
    printf("\n\n\n");

    foreach ($params as $key => $value)
    {
        printf("$key : [$value]\n");
    }
}

function setDNS($master, $dns)
{
    global $WS_URL, $DL_URL;

    $dns_project = 'compute-engine-vm-test';
    $dns_zone = 'wintech-thai';
    $baseDNS = "$dns.wintech-thai.com";

    $obj = getInstanceInfo($master);

    $networks = $obj->{'networkInterfaces'};
    $accesssCfgs = $networks[0]->{'accessConfigs'};
    $externalIP = $accesssCfgs[0]->{'natIP'};

    $start_cmd = "gcloud dns --project=$dns_project record-sets transaction start --zone=$dns_zone";
    executeCommand($start_cmd);

    #$rm_cmd = "gcloud dns --project=$dns_project record-sets transaction remove $externalIP --name=$baseDNS. --ttl=300 --type=A --zone=$dns_zone";
    #executeCommand($rm_cmd);

    $add_cmd = "gcloud dns --project=$dns_project record-sets transaction add $externalIP --name=$baseDNS. --ttl=300 --type=A --zone=$dns_zone";
    executeCommand($add_cmd);

    $end_cmd = "gcloud dns --project=$dns_project record-sets transaction execute --zone=$dns_zone";
    executeCommand($end_cmd);

    setParam('DNS', $baseDNS);
    setParam('WS_URL', sprintf($WS_URL, $baseDNS));
    setParam('DL_URL', sprintf($DL_URL, $baseDNS));
}

function initDB($master)
{
    global $PRODUCT_NAME, $USER_NAME, $USER_PASSWORD;

    $cmd = <<<EOT
sudo docker exec -i $(sudo docker ps -q -f name="%s_apache" -n 1) \
/wis/system/bin/init_script.bash \
/onix/dev/wtt/test/dispatcher.php \
%s \
%s
EOT;

    $cmd = sprintf($cmd, $PRODUCT_NAME, $USER_NAME, $USER_PASSWORD);
    sshExecute($master, [$cmd], '');
}

function deployApp($master, $storage, $product)
{
    global $HOME_DIR;

    $ip = getInstanceIP($storage);
    $vars = [ 
        '${NFS_IP}' => $ip,
        '${ROOT_MOUNT}' => '/wtt',
        '${REPLICA}' => '10',
    ];

    $cmd = "git clone https://pjamenaja@bitbucket.org/pjamenaja/docker.git";
    executeCommand($cmd);

    $template = "./docker/app_$product" . "_nfs.yml";
    $newFile = "app_$product.yml";
    substituteVar($template, $vars, $newFile);

    $destFile = "$HOME_DIR/$newFile";
    sshCopy([$newFile], $master, "$destFile");

    $cmd = "sudo docker stack deploy -c $destFile $product";
    sshExecute($master, [$cmd], '');

    unlink($newFile);
}

function getInstanceInfo($name)
{
    global $PROJECT_ID;

    #$cmd = "gcloud compute instances list --filter=\"name=('$name')\" --format json > $jsonFile";
    #executeCommand($cmd);

    $data = new CTable('');
    $data->setFieldValue('ID', $name);
    $data->setFieldValue('PROJECT', $PROJECT_ID);

    list($result, $lines, $json) = GcpVM::GetVMInfo($data);
    if ($result != 0)
    {
        print($json);
        exit(1);
    }

    return($json);
}

function getInstanceIP($name)
{
    $obj = getInstanceInfo($name);

    $networks = $obj->{'networkInterfaces'};
    $ip = $networks[0]->{'networkIP'};

    return($ip);
}

function substituteVar($template, $vars, $newFile)
{
    $fh = fopen($template, 'r');
    if (!$fh)
    {   
        throw new Exception("Unable to open file [$template]!!!!");
    }
    
    $cmd = '';
    $lines = [];

    while ($row = fgets($fh))
    {   
        $line = $row;
        foreach ($vars as $key => $value)
        {
           $line = str_replace($key, $value, $line); 
        }

        array_push($lines, $line);
    }
    
    $op = fopen($newFile, 'w');
    foreach ($lines as $line)
    {
        fwrite($op, $line);
    }
    fclose($op);
}

function initSwarm($masters, $workers)
{
    global $HOME_DIR;

    $fname = "swarm.tmp";
    $tempFile = "$HOME_DIR/$fname";

    $master = $masters[0]; #Only the first one

    printf("Starting swarm master [$master] ...\n");

    $cmd = "sudo docker swarm init ";
    sshExecute($master, [$cmd], $tempFile);

    sshDownload([$tempFile], $master, '.');
    $cmd = getDockerJoinCommand($fname);

    foreach ($workers as $worker)
    {
        sshExecute($worker, [$cmd], '');
    } 

    unlink($fname);
}

function getDockerJoinCommand($file)
{
    $fh = fopen($file, 'r');
    if (!$fh)
    {
        throw new Exception("Unable to open file [$file]!!!!");
    }

    $cmd = '';
    while ($row = fgets($fh))
    {
        # "sudo docker swarm join --token SWMTKN-1-5am3omyv2kmyzfwrpq522vfvn9krbbbb48xw2qnn24o5zi0c5c-92a79rzshafsmmxwp6svoknyc 10.140.0.3:2377";
        if (preg_match('/join --token/', $row))
        {
            $cmd = sprintf("sudo %s", trim($row));
            return($cmd);
        }
    }

    return($cmd);
}

function initialConfig($instance, $cfg)
{
    $src = "/tmp/$cfg";
    $dst = "/wtt/onix/system/config/$cfg";

    createConfigFile($instance, $cfg);
    sshCopy([$cfg], $instance, '/tmp');
    sshExecute($instance, ["sudo cp $src $dst; sudo chmod 600 $dst; sudo chown onix:onix $dst"], '');
}

function setParam($key, $value)
{
    global $PARAM;
    $PARAM{$key} = $value;
}

function createConfigFile($instance, $fname)
{
    printf("Creating config file [$fname] ...\n");
        
    $dtm = date("m/d/Y h:i:sa");
    $dsn = "PDO:Pg:dbname=postgres;host=postgres;port=5432";

    $pid = getmypid();
    $pw = password_hash('DO NOT TRY TO HACK!!!!' . $pid, PASSWORD_DEFAULT);
    $key = substr(md5($pw), 0, 16);

    setParam('KEY', $key);

    $config = <<<EOT
#IBSRevision : 1.0 ($dtm)
#FILE    : $fname
#AUTHOR  : Auto generated by WTT script
#PURPOSE : API Config file per client.
        
[DEFAULT]

DSN=$dsn
DB_TYPE=POSTGRESQL
USERNAME=postgres
PASSWORD=
        
#!!!!! Be sure to set pg_hba.conf to use 'password' authentication method !!!!!!
        
#This is symetric key for client and server
KEY=$key

EOT;

    //printf("$config\n");

    $fh = fopen($fname, 'w');
    fwrite($fh, $config);
    fclose($fh);
}

function createStorageInstance($instance)
{
    global $HOME_DIR;

    $instances = createInstance($instance, 'storage');

#    foreach ($instances as $name)
#    {
#        sshCopy(['./server_master_setup.bash', './server_storage_setup.bash'], $name, $HOME_DIR);
#        sshExecute($name, ["$HOME_DIR/server_storage_setup.bash"], '');
#    }

    return($instances);
}

function createLBInstances($instances)
{
    global $HOME_DIR;

    $temps = [];
    foreach ($instances as $instance)
    {
        list($name) = createInstance($instance, 'master');
        array_push($temps, $name);

#        sshCopy(['./server_master_setup.bash'], $name, $HOME_DIR);
#        sshExecute($name, ["$HOME_DIR/server_master_setup.bash"], '');
    }

    return($temps);
}

function sshDownload($srcs, $instance, $target)
{
    global $ZONE;

    $dest = "$target";

    foreach ($srcs as $src)
    {
        printf("Downloading file from [$instance:$src] to [$dest]...\n\n");

        $data = new CTable('');
        $data->setFieldValue('ID', $instance);
        $data->setFieldValue('ZONE', $ZONE);
        $data->setFieldValue('SOURCE', $src);
        $data->setFieldValue('TARGET', $target);

        list($result, $lines, $output) = GcpSSH::SSHDownload($data);
        if ($result != 0)
        {
            print($output);
            exit(1);
        }

#        $cmd = "gcloud compute scp $instance:$src $dest --zone=$ZONE --strict-host-key-checking=no";
#        executeCommand($cmd);
    }
}

function sshCopy($srcs, $instance, $target)
{
    global $ZONE;

    $dest = "$instance:$target";

    foreach ($srcs as $src)
    {
        printf("Sending file [$src] to [$dest]...\n\n");

        $data = new CTable('');
        $data->setFieldValue('ID', $instance);
        $data->setFieldValue('ZONE', $ZONE);
        $data->setFieldValue('SOURCE', $src);
        $data->setFieldValue('TARGET', $target);

        list($result, $lines, $output) = GcpSSH::SSHCopy($data);
        if ($result != 0)
        {
            print($output);
            exit(1);
        }
    }
}

function sshExecute($instance, $scripts, $tmpFile)
{
    global $ZONE;

    foreach ($scripts as $script)
    {
        $command = "'$script 1>&2'";
        if ($tmpFile != '')
        {
            $command = "'$script > $tmpFile'";
        }
        
        printf("Executing command [$script] at [$instance]...\n\n");

        $data = new CTable('');
        $data->setFieldValue('ID', $instance);
        $data->setFieldValue('ZONE', $ZONE);
        $data->setFieldValue('COMMAND', $command);
        
        list($result, $lines, $output) = GcpSSH::SSHExecute($data);
        if ($result != 0)
        {   
            print($output);
            exit(1);
        }
    }
}

function createInstance($instance, $imageKey)
{
    global $STORAGE_INSTANCE, $PROJECT_ID, $ZONE, $IMAGE, $IMAGE_PROJECT, $CUSTOM_IMAGES;
    global $SHORT_NAME, $LONG_NAME, $PRODUCCT_NAME, $STAGE, $PRODUCT_NAME, $DELAY_SEC;

    list($key, $cpu, $disk, $type) = explode(':', $instance);
    $instanceName = "$PRODUCT_NAME-$STAGE-$SHORT_NAME-$LONG_NAME-$key";

    printf("Creating instance [$instanceName]...\n\n");

    $imageName = $CUSTOM_IMAGES{$imageKey};

    $data = new CTable('');
    $data->setFieldValue('ID', $instanceName);
    $data->setFieldValue('PROJECT', $PROJECT_ID);
    $data->setFieldValue('ZONE', $ZONE);
    $data->setFieldValue('IMAGE', $imageName);
    $data->setFieldValue('IMAGE_PROJECT', $PROJECT_ID);
    $data->setFieldValue('BOOT_DISK_DEVICE_NAME', $instanceName);

    list($result, $lines, $output) = GcpVM::CreateVM($data);
    if ($result != 0)
    {
        print($output);
        exit(1);
    }

    sleep($DELAY_SEC);

    return([$instanceName]);
}

function executeCommand($cmd)
{
    printf("$cmd\n\n");
    exec($cmd);
}

?>
