#!/bin/bash

sudo yum update -y
sudo yum install -y nfs-utils
sudo yum install -y git
sudo groupadd -g 69690 onix
sudo useradd -u 69690 -g 69690 onix
sudo yum install -y yum-utils device-mapper-persistent-data lvm2
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
sudo yum install -y docker-ce
sudo systemctl start docker
sudo systemctl enable docker
sudo systemctl restart docker

sudo firewall-cmd --permanent --zone=public --add-service=http
sudo firewall-cmd --permanent --zone=public --add-service=https
sudo firewall-cmd --reload
sudo systemctl restart firewalld

sudo systemctl enable nfslock
sudo systemctl start nfslock

