#!/bin/bash

./server_master_setup.bash

#sudo  yum -y install nfs-utils
sudo yum install -y nfs-common
sudo systemctl enable nfs-server.service
sudo systemctl start nfs-server.service

sudo mkdir -p /wtt/onix /wtt/pgdata /wtt/gcloud /wtt/develop
sudo mkdir -p /wtt/onix/log /wtt/onix/wip /wtt/onix/lock /wtt/onix/session /wtt/onix/download /wtt/onix/windows /wtt/onix/system/config 

sudo chown onix:onix -R /wtt

sudo echo '/wtt        *(rw,sync,anonuid=69690,anongid=69690)' > /tmp/exports
sudo cp /tmp/exports /etc/exports
sudo exportfs -a

sudo firewall-cmd --permanent --zone=public --add-service=nfs
sudo firewall-cmd --reload

